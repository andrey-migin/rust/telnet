
use std::{env, sync::Arc};
use async_std::{io, net::TcpStream};
use futures::{AsyncReadExt, AsyncWriteExt};

#[async_std::main]
async fn main() {
    
    let args: Vec<String> = env::args().collect();

    let host_port:String;

    if args.len() == 3{
        host_port = format!("{}:{}", args[1], args[2]);
    }
    else
    if args.len() == 2{
        host_port = format!("{}", args[1]);
    }
    else{
        println!("Please specify the end point to connect. Ex: telnet localhost:5000");
        return;
    }
    

    let stream = TcpStream::connect(&host_port).await;

    if stream.is_err(){
        println!("Could not connect to: {}", host_port);
        return;
    }


    let tcp_stream = Arc::new(stream.unwrap()) ;

    async_std::task::spawn(read_thread(tcp_stream.clone()));

    let mut tcp_stream: &TcpStream = &*tcp_stream;

    let mut stdin = io::stdin();
    let mut buf:[u8; 1] =  [0];

    loop  {
       
        let console_read_result = stdin.read(&mut buf).await;

        if console_read_result.is_err(){
            let err = console_read_result.unwrap_err();
            println!();
            println!("Can not read from console: {}", err);
        }

        let write_result = tcp_stream.write_all(&buf).await;

        if write_result.is_err(){
            let err = write_result.unwrap_err();
            println!();
            println!("Can not write to the socket: {}", err);
        }
        
    }
}


async fn read_thread(tcp_stream: Arc<TcpStream>){
    let mut line:[u8; 1024] = [0; 1024];
    let mut tcp_stream: &TcpStream = &*tcp_stream;

    loop  {
        let result = tcp_stream.read(&mut line).await;
    
        if result.is_err(){
            println!("Can not read from the tcp stream. Disconnecting");
            return;
        }

        let read_size = result.unwrap();

        let result_vec:Vec<u8> = Vec::from(&line[0..read_size]);


        let str = String::from_utf8(result_vec);

        if str.is_ok(){
            print!("{}", str.unwrap());
        }

    }

}